//
//  main.m
//  MojiKakudai
//
//  Created by ham on 13/09/01.
//  Copyright (c) 2013年 ham. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
