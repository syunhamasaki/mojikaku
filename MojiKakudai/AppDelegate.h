//
//  AppDelegate.h
//  MojiKakudai
//
//  Created by ham on 13/09/01.
//  Copyright (c) 2013年 ham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
